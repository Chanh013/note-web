import { acceptHMRUpdate, defineStore } from "pinia";
import { camelCaseKeys } from "~/common/utils/Transform";
import { Sticky } from "~/home/dtos/Sticky.dto";
import StickyService from "../services/StickyService";

export const useStickyStore = defineStore(
  "sticky",
  () => {
    const stickyData = ref<Sticky[]>([]);
    const isUpdate = ref(true);

    async function load() {
      stickyData.value = [];
      const result = camelCaseKeys(
        Sticky,
        await StickyService.getStickys()
      ) as Sticky[];
      if (result) {
        stickyData.value = result;
      }
    }

    return {
      isUpdate,
      stickyData,
      load,
    };
  },
  {
    persist: true,
  }
);
if (import.meta.hot)
  import.meta.hot.accept(acceptHMRUpdate(useStickyStore, import.meta.hot));
