import BaseService, { METHODS } from "~/common/services/BaseService";

class StickyService extends BaseService {
  constructor(prefix: string) {
    super(prefix);
  }

  async getStickys() {
    return await this.performRequest(METHODS.GET, "sticky/stickys");
  }

}
export default new StickyService("");
