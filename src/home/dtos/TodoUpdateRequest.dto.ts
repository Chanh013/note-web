import { Expose } from "class-transformer";
import { Note } from "./Note.dto";

export class TodoUpdateRequest {
  @Expose({ name: "todo_id" })
  todoId?: number | string;

  todoUuid?: string;
  label = "";
  status = 0;
  trim? = 0;
  @Expose({ name: "is_delete" })
  isDelete?: boolean;

  note? = new Note();
}
