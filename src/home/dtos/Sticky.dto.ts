import { Expose } from "class-transformer";

export class Sticky {
  id = "";
  content = "";
  uuid = "";
  cover = "";
  position = "";
  minimize: boolean = false;
  index: number = 0;
  @Expose({ name: "created_at" })
  createdAt = "";
}
