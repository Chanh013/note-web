import { Expose } from "class-transformer";
import { Note } from "./Note.dto";

export class Todo {
  @Expose({ name: "todo_id" })
  todoId?: number | string;

  todoUuid?: string;
  label = "";
  status = 0;
  trim? = 0;
  note? = new Note();
}
