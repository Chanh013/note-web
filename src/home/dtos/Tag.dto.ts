import { Expose } from "class-transformer";

export class Tag {
  @Expose({ name: "tag_id" })
  tagId?: number;

  id?: number;
  label = "";
  cover = "";
  color = "";
  notes: Node[] = [];
}
