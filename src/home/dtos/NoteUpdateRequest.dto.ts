import { Expose } from "class-transformer";
import { TodoUpdateRequest } from "./TodoUpdateRequest.dto";
import { TagUpdateRequest } from "./TagUpdateRequest.dto";

export class NoteUpdateRequest {
  @Expose({ name: "note_id" })
  noteId? = 0;

  title = "";
  @Expose({ name: "time_from" })
  timeFrom = "";

  @Expose({ name: "time_to" })
  timeTo = "";

  description = "";
  cover = "";
  todos: TodoUpdateRequest[] = [];
  tags: TagUpdateRequest[] = [];
}
