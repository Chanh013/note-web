import { Expose } from "class-transformer";
import { Todo } from "./Todo.dto";
import { Tag } from "./Tag.dto";

export class Note {
  @Expose({ name: "note_id" })
  noteId = 0;

  title = "";
  @Expose({ name: "time_from" })
  timeFrom = "";

  @Expose({ name: "time_to" })
  timeTo = "";

  description = "";
  cover = "";
  todos: Todo[] = [];
  tags: Tag[] = [];
  @Expose({ name: "created_at" })
  createdAt = "";
}
