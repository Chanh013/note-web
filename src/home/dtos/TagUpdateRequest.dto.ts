import { Expose } from "class-transformer";

export class TagUpdateRequest {
  @Expose({ name: "tag_id" })
  tagId?: number;

  id?: number;
  label = "";
  cover = "";
  color = "";
  @Expose({ name: "is_delete" })
  isDelete?: boolean;

  notes: Node[] = [];
}
