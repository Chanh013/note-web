import BaseService, { METHODS } from "~/common/services/BaseService";
import { Note, Tag } from "../dtos";
import { NoteUpdateRequest } from "../dtos/NoteUpdateRequest.dto";

class NoteService extends BaseService {
  constructor(prefix: string) {
    super(prefix);
  }

  async getNotes() {
    return await this.performRequest(METHODS.GET, "note/notes");
  }

  async getNote(idNote: string | number) {
    return await this.performRequest(METHODS.GET, `note/note/${idNote}`);
  }

  async postNote(data: Note) {
    return await this.performRequest(METHODS.POST, "note/create", data);
  }

  async removeNote(removeNoteDto: { idNote: string }) {
    return await this.performRequest(
      METHODS.POST,
      "note/delete",
      removeNoteDto
    );
  }

  async updateNote(idNote: string, data: NoteUpdateRequest) {
    return await this.performRequest(
      METHODS.PUT,
      `note/update/${idNote}`,
      data
    );
  }

  async getTags() {
    return await this.performRequest(METHODS.GET, "tag/tags");
  }

  async postTag(data: Tag) {
    return await this.performRequest(METHODS.POST, "tag/create", data);
  }

  async updateTag(data: Tag) {
    return await this.performRequest(
      METHODS.PUT,
      `tag/update/${data.tagId}`,
      data
    );
  }

  async removeTag(tagId: string) {
    return await this.performRequest(METHODS.POST, `tag/remove/${tagId}`);
  }
}
export default new NoteService("");
