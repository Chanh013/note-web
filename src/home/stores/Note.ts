import { acceptHMRUpdate, defineStore } from "pinia";
import { Note, Tag } from "../dtos";
import { camelCaseKeys } from "~/common/utils/Transform";
import NoteService from "../services/NoteService";
import { NoteUpdateRequest } from "../dtos/NoteUpdateRequest.dto";

export const useNoteStore = defineStore(
  "note",
  () => {
    const noteData = ref<Note[]>([]);
    const noteDetail = ref<Note>();
    const tagData = ref<Tag[]>([]);

    async function load() {
      noteData.value = [];
      const result = camelCaseKeys(
        Note,
        await NoteService.getNotes()
      ) as Note[];
      if (result) {
        noteData.value = result;
      }
    }

    async function loadNote(idNote: string | number) {
      noteDetail.value = new Note();
      const result = camelCaseKeys(
        Note,
        await NoteService.getNote(idNote)
      ) as Note;
      if (result) {
        noteDetail.value = result;
      }
    }

    async function postNote(data: Note) {
      const result = await NoteService.postNote(data);
      return result;
    }

    async function removeNote(data: { idNote: string }) {
      try {
        const result = await NoteService.removeNote(data);
        return result;
      } catch (error) {
        console.log(error);
      }
    }

    async function updateNote(idNote: string, data: NoteUpdateRequest) {
      const body = camelCaseKeys(NoteUpdateRequest, data) as NoteUpdateRequest;
      const result = await NoteService.updateNote(idNote, body);
      return result;
    }

    async function loadTags() {
      tagData.value = [];
      const result = camelCaseKeys(Tag, await NoteService.getTags()) as Tag[];
      if (result) {
        tagData.value = result;
      }
    }

    async function postTag(data: Tag) {
      const result = await NoteService.postTag(data);
      return result;
    }

    async function updateTag(data: Tag) {
      const result = await NoteService.updateTag(data);
      return result;
    }

    async function removeTag(tagId: string) {
      const result = await NoteService.removeTag(tagId);
      return result;
    }

    return {
      noteData,
      tagData,
      noteDetail,
      load,
      loadNote,
      loadTags,
      postNote,
      removeNote,
      updateNote,
      postTag,
      updateTag,
      removeTag,
    };
  },
  {
    persist: true,
  }
);
if (import.meta.hot)
  import.meta.hot.accept(acceptHMRUpdate(useNoteStore, import.meta.hot));
