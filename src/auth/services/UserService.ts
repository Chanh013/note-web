import BaseService, { METHODS } from "~/common/services/BaseService";
import { User } from "../dtos";
import { MailRequest } from "../dtos/Mail.dto";

class UserService extends BaseService {
  constructor(prefix: string) {
    super(prefix);
  }

  async login(user: User) {
    return await this.performRequest(METHODS.POST, "user/login", user);
  }

  async register(user: User) {
    return await this.performRequest(METHODS.POST, "user/register", user);
  }

  async checkEmail(user: User) {
    return await this.performRequest(METHODS.POST, "user/check-email", user);
  }

  async sendEmail(mail: MailRequest) {
    return await this.performRequest(METHODS.POST, "user/send-mail", mail);
  }

  async getUser(): Promise<User> {
    return await this.performRequest(METHODS.GET, "user/user-infor");
  }

  async uploadBackground(file: { url: string }) {
    return await this.performRequest(
      METHODS.POST,
      "user/upload-background",
      file
    );
  }

  async uploadAvatar(file: { url: string }) {
    return await this.performRequest(METHODS.POST, "user/upload-avatar", file);
  }

  async updateProfile(user: User) {
    return await this.performRequest(
      METHODS.PUT,
      `user/update/${user.id}`,
      user
    );
  }

  async updatePassword(request: {
    userId: string;
    currentPassword: string;
    newPassword: string;
  }) {
    return await this.performRequest(
      METHODS.PUT,
      `user/update-password/${request.userId}`,
      request
    );
  }
}
export default new UserService("");
