import { acceptHMRUpdate, defineStore } from "pinia";
import { User } from "../dtos";
import UserService from "../services/UserService";
import { MailRequest } from "../dtos/Mail.dto";
import { camelCaseKeys, snakeCaseKeys } from "~/common/utils/Transform";

export const useAuthStore = defineStore(
  "auth",
  () => {
    const jwt = ref<string>("");
    const user = ref<User | undefined>(undefined);

    async function login(data: User) {
      const result = (await UserService.login(data)) as any;
      if (result) {
        jwt.value = result["access_token"];
      }
      return result;
    }

    async function register(data: User) {
      const user = (await UserService.register(
        snakeCaseKeys(User, data) as User
      )) as User;
      if (user) {
        return login({
          email: user.email,
          password: data.password,
        });
      }
    }

    async function checkEmail(data: User) {
      return await UserService.checkEmail(data);
    }

    async function sendEmail(mail: MailRequest) {
      return await UserService.sendEmail(mail);
    }

    async function getUser() {
      const result = await UserService.getUser();
      if (result) {
        user.value = camelCaseKeys(User, result) as User;
      }
    }

    async function uploadBackground(file: { url: string }) {
      const result = await UserService.uploadBackground(file);
      if (result) {
        user.value = camelCaseKeys(User, result) as User;
      }
    }

    async function uploadAvatar(file: { url: string }) {
      const result = await UserService.uploadAvatar(file);
      if (result) {
        user.value = camelCaseKeys(User, result) as User;
      }
    }

    async function updateProfile(data: User) {
      await UserService.updateProfile(snakeCaseKeys(User, data) as User);
    }

    async function updatePassword(request: {
      userId: string;
      currentPassword: string;
      newPassword: string;
    }) {
      return await UserService.updatePassword(request);
    }
    return {
      jwt,
      user,
      login,
      checkEmail,
      register,
      sendEmail,
      getUser,
      uploadBackground,
      uploadAvatar,
      updateProfile,
      updatePassword,
    };
  },
  {
    persist: true,
  }
);
if (import.meta.hot)
  import.meta.hot.accept(acceptHMRUpdate(useAuthStore, import.meta.hot));
