import { Expose } from "class-transformer";

export class User {
  @Expose({ name: "id" })
  id? = 0;

  @Expose({ name: "full_name" })
  fullName? = "";

  email = "";
  password = "";
  avatar? = "";
  background? = "";
  @Expose({ name: "created_at" })
  createdAt? = "";
}
