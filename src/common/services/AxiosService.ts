import axios, { AxiosRequestHeaders } from "axios";
import { useSnackbars } from "../stores/Snackbars";

const http = axios.create({
  baseURL: import.meta.env.VITE_API_URL,
});

http.interceptors.request.use(async (config) => {
  const auth = localStorage.getItem("auth");
  const header = {
    Accept: "application/json",
    Authorization: `Bearer ${auth ? JSON.parse(auth)["jwt"] : ""}`,
  } as AxiosRequestHeaders;

  config.headers = header;
  return config;
});

http.interceptors.response.use(undefined, function (error) {
  if (error) {
    if (error.response) {
      if (error.response.status === 401) {
        localStorage.removeItem("auth");
        location.assign("/login");
      }
      useSnackbars().openSnackbar("error", error.response.data.message);
    } else {
      useSnackbars().openSnackbar("error", error.message);
    }
  }
});

export default http;
