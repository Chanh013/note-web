export function bodyMail(otp: string, title: string, content: string) {
  return `
    <div
  style="display:flex; flex-direction: column; align-items:center;font-family:sans-serif;margin:0;padding:0;justify-content: center">
  <div style="margin-top: 20px; width: 100%; background-color: #fff; padding: 40px; border-radius: 8px">
    <h2
      style="font-weight:700; text-align:center; font-size: 24px; color:#4D4D4D;margin-bottom:0px !important;margin-top: 0px !important">
      ${title}</h2>
    <p
      style="text-align:center; font-size: 14px; font-weight: 400;color:#4D4D4D; margin-bottom:0px !important; margin-top: 10px !important">
      ${content}</p>
    <p
      style="text-align:center; font-size: 14px; font-weight: 400;color:#4D4D4D; margin-bottom:0px !important; margin-top: 5px !important">
      Please you the verification code below on the Note Todo website:</p>
    <div style="width: 100%;text-align:center; margin-top: 20px; margin-bottom: 20px">
      <div
        style="font-size:18px; padding:10px 12px; background-color:#B148C8;display:inline-flex;;color:white;justify-content:center;align-items:center; text-align:center ">
        ${otp}
      </div>
    </div>
    <p style="text-align:center; margin: 0px; font-size: 15px;color:#4D4D4D;">Thanks you!</p>
    <p style="text-align:center; margin: 5px; font-size: 18px; font-weight: 700">Nguyễn Minh Chánh</p>
  </div>
</div>
    `;
}
