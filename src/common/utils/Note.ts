import { Todo } from "~/home/dtos";

export class NoteUtil {
  private static instance: NoteUtil;

  static getInstance() {
    this.instance = this.instance ?? new NoteUtil();
    return this.instance;
  }

  getPercent(listTodosCheck: Todo[], listTodos: Todo[]) {
    return Math.floor((listTodosCheck.length * 100) / listTodos.length);
  }
}
