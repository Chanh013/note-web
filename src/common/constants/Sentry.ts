export type SeverityLevel = 'fatal' | 'error' | 'warning' | 'info' | 'debug';
interface SentryLevel {
  [key: string]: {
    value: number;
    name: SeverityLevel;
  };
}

export const SENTRY_LEVEL: SentryLevel = {
  FATAL: {
    value: 5,
    name: 'fatal',
  },
  ERROR: {
    value: 4,
    name: 'error',
  },
  WARNING: {
    value: 3,
    name: 'warning',
  },
  INFO: {
    value: 2,
    name: 'info',
  },
  DEBUG: {
    value: 1,
    name: 'debug',
  },
};
