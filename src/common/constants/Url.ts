export const ALLOWED_VISIBLE_SCREEN_BY_ROLE = {
  admin: [
    'index',
    'history',
    'setting',
    'device-managements',
    'device-managements-serialNo-edit',
    'certifications',
    'device-log-serialNo',
    'location-log-serialNo',
    'batch-setup',
  ],
  client: ['index', 'history'],
};
