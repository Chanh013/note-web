type ItemMenu = {
  label: string;
  icon: string;
  key: "logout" | "setting" | "account";
};

export const MENU: ItemMenu[] = [
  { label: "Profile", icon: "mdi-account-outline", key: "account" },
  { label: "Settings", icon: "mdi-cog-outline", key: "setting" },
  { label: "Logout", icon: "mdi-logout", key: "logout" },
];
