type OverViewType = {
  label: string;
  icon: string;
  color: string;
};
export const OverView: OverViewType[] = [
  {
    color: "text-gray-500",
    icon: "mdi-box-shadow",
    label: "Todo",
  },
  {
    color: "text-[#982BFF]",
    icon: "mdi-box-shadow",
    label: "In progress",
  },
  {
    color: "text-[#22C55E]",
    icon: "mdi-box-shadow",
    label: "Done",
  },
  {
    color: "text-[#FF2B2B]",
    icon: "mdi-box-shadow",
    label: "Late",
  },
];
