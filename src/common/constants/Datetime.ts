/**
 * Date format: `YYYY/MM/DD`
 */
export const DATE_FORMAT = "dd/MM/yyyy";
/**
 * Time format: `HH:mm:ss`
 */
export const TIME_SECOND_FORMAT = "HH:mm:ss";
export const TIME_FORMAT = "HH:mm";

/**
 * DateTime format: `YYYY/MM/DD HH:mm:ss`
 */
export const DATETIME_FORMAT = `${TIME_FORMAT} ${DATE_FORMAT}`;

export const DATETIME_LENGTH = 16;
