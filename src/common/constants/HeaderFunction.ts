export const HeaderFunction: Record<string, any> = {
  note: {
    param: "/",
    function: [
      { name: "Note", path: "/", icon: "mdi-pencil-outline" },
      { name: "Calendar", path: "/note/calendar", icon: "mdi-calendar-week-outline" },
      { name: "Sticky", path: "/note/sticky", icon: "mdi-note-edit" },
      { name: "History", path: "/note/history", icon: "mdi-clipboard-text-clock-outline" },
    ],
  },
  music: {
    param: "/music",
    function: [
      { name: "Music", path: "/music", icon: "mdi-music" },
    ],
  },
};
