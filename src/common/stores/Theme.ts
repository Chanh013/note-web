import { acceptHMRUpdate, defineStore } from "pinia";

export const useTheme = defineStore(
  "theme",
  () => {
    const theme = ref<boolean>(false);

    const changeTheme = (typeTheme?: boolean) => {
      theme.value = typeTheme ?? !theme.value;
    };

    return {
      theme,
      changeTheme,
    };
  },
  {
    persist: true,
  }
);
if (import.meta.hot)
  import.meta.hot.accept(acceptHMRUpdate(useTheme, import.meta.hot));
