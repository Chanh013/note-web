import { acceptHMRUpdate, defineStore } from "pinia";

export const useOverLoading = defineStore("overloading", () => {
  const overLoading = ref(false);

  const openOverLoading = () => {
    overLoading.value = true;
  };

  const closeOverLoading = () => {
    overLoading.value = false;
  };

  return {
    overLoading,
    openOverLoading,
    closeOverLoading,
  };
});
if (import.meta.hot)
  import.meta.hot.accept(acceptHMRUpdate(useOverLoading, import.meta.hot));
