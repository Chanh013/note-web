import { acceptHMRUpdate, defineStore } from "pinia";

export type SnackbarsType = {
  isOpen: boolean;
  text: string;
  status?: "success" | "error" | "warning";
};

export const useSnackbars = defineStore(
  "snackbars",
  () => {
    const snackbars = ref<SnackbarsType>({ isOpen: false, text: "" });

    const openSnackbar = (
      status: "success" | "error" | "warning",
      text: string
    ) => {
      snackbars.value = {
        isOpen: true,
        text,
        status,
      };
    };

    const closeSnackbar = () => {
      snackbars.value = {
        isOpen: false,
        text: "",
      };
    };

    return {
      snackbars,
      openSnackbar,
      closeSnackbar
    };
  },
  {
    persist: true,
  }
);
if (import.meta.hot)
  import.meta.hot.accept(acceptHMRUpdate(useSnackbars, import.meta.hot));
