import { ProCalendar } from "vue-pro-calendar";
import { UserModule } from "~/types";
import "vue-pro-calendar/style";

export const install: UserModule = ({ app }) => {
  app.use(ProCalendar);
};
